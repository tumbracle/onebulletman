// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "BasicEnemyCharacter.generated.h"

class UBehaviorTree;
class UPaperFlipbookComponent;
enum class EHandState : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShoot, ABullet*, SpawnedBullet);

UCLASS()
class ONEBULLETMAN_API ABasicEnemyCharacter : public APaperCharacter
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	ABasicEnemyCharacter();

	UPROPERTY(BlueprintAssignable)
	FOnShoot OnShoot;
	
	UPROPERTY(BlueprintReadWrite)
	EHandState HandCondition;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Sprites")
	UPaperFlipbookComponent* Hand;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	FVector2D Directionality = FVector2D(0, -1);

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	USceneComponent* HandSocket;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	UCapsuleComponent* HorCollision;

	UPROPERTY()
	ABullet* BulletRef;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ABullet> BulletType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Health;
	
protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UBehaviorTree* BHTree;

	UPROPERTY()
	FTimerHandle ReloadTimer;

public:
	UBehaviorTree* const GetBehaivorTree();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void AIReload(float ReloadTime);

	UFUNCTION()
	void FinishReload();

	void Shoot();

	void SetAIDirection();

	virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent,
	AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
	void SpawnParticle(AActor* Damager);
	void SpawnParticle_Implementation(AActor* Damager);
};


