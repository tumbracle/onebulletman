// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/PlayerCameraManager.h"
#include "PixelPerfectPlayerCameraManager.generated.h"

/**
 * 
 */
UCLASS()
class ONEBULLETMAN_API APixelPerfectPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()
	
public:
	virtual void DoUpdateCamera(float DeltaTime) override;
};
