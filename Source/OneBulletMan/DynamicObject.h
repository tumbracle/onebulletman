// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DynamicObject.generated.h"

class USphereComponent;
class UPaperFlipbookComponent;

UCLASS()
class ONEBULLETMAN_API ADynamicObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADynamicObject();

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Sprites")
	UPaperFlipbookComponent* BodySprite;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	USphereComponent* BodyCollision;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
