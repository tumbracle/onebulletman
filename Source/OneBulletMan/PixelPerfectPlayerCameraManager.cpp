// Fill out your copyright notice in the Description page of Project Settings.


#include "PixelPerfectPlayerCameraManager.h"

void APixelPerfectPlayerCameraManager::DoUpdateCamera(float DeltaTime)
{
	// Update the camera
	Super::DoUpdateCamera(DeltaTime);

	// Snap the final camera location to the pixel grid
	{
		const float PixelsPerUnits = 0.25f;
		const float UnitsPerPixel = 1.0f / PixelsPerUnits;

		FMinimalViewInfo CameraCachePOV = GetCameraCacheView();
		CameraCachePOV.Location.X = FMath::GridSnap(CameraCachePOV.Location.X, UnitsPerPixel);
		CameraCachePOV.Location.Z = FMath::GridSnap(CameraCachePOV.Location.Y, UnitsPerPixel);
		FillCameraCache(CameraCachePOV);
	}
}