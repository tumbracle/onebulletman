// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "SampleGameViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class ONEBULLETMAN_API USampleGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

public:
	virtual void Activated(FViewport* InViewport, const FWindowActivateEvent& InActivateEvent) override;
};
