// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

#include "Jack.h"
#include "PaperSpriteComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(SphereCollision);
	Sprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("Body"));
	Sprite->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	Enemy = UGameplayStatics::GetPlayerPawn(GetWorld(), 0) != GetInstigator(); 
	MovementVector = Sprite->GetForwardVector() * 500;
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ABullet::SpawnParticle_Implementation()
{
	
}

//Moving
void ABullet::Move()
{
	FHitResult Hit;
	AddActorWorldOffset(MovementVector * GetWorld()->GetDeltaSeconds(), true, &Hit);
	if(Hit.bBlockingHit)
	{
		const FRotator ParticleRot = UKismetMathLibrary::MakeRotFromXY(Hit.Normal * -1, Hit.Normal * -1);
		GetWorld()->SpawnActor<AActor>(HitActor, FTransform(FRotator(0, ParticleRot.Yaw, 0), GetActorLocation()));
		MovementVector = UKismetMathLibrary::GetReflectionVector(MovementVector, FVector(Hit.Normal.X, Hit.Normal.Y, 0));
		Sprite->SetWorldRotation(UKismetMathLibrary::MakeRotFromXY(MovementVector,MovementVector));
		if(Hit.GetActor() != GetInstigator())
		{
			AController* PlayerContr = GetInstigator()->GetController();
			UGameplayStatics::ApplyDamage(Hit.GetActor(), Type == EBulletType::Fast ? Damage + 1 : Damage, PlayerContr, this, TypeOfDamage);
			if(Hit.GetActor() && Hit.GetActor()->ActorHasTag(FName("BulletDamager")))
			{
				AddDamage();
			}
			else
			{
				RestoreHealth();
			}
		}
		else
			RestoreHealth();
		if(Enemy)
			Destroy();
	}
}

void ABullet::AddDamage()
{
	Health--;
	DamageReaction(false);
	if(Health == 0)
	{
		AJack* CharRef = Cast<AJack>(GetInstigator());
		CharRef->BulletsStore++;
		CharRef->PistolReady();
		UGameplayStatics::ApplyDamage(CharRef, 1, GetInstigator()->GetController(), this, TypeOfDamage);
		Destroy();
	}
}

void ABullet::RestoreHealth()
{
	DamageReaction(true);
	Health = MaxHealth;
}

void ABullet::DamageReaction_Implementation(bool Restore)
{}

