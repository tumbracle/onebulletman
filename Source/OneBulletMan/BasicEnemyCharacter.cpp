// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicEnemyCharacter.h"

#include "Jack.h"
#include "PaperFlipbookComponent.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Bullet.h"

ABasicEnemyCharacter::ABasicEnemyCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	HorCollision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HorizontalCollision"));
	HorCollision->SetupAttachment(RootComponent);
	HandSocket = CreateDefaultSubobject<USceneComponent>(TEXT("Socket"));
	HandSocket->SetupAttachment(RootComponent);
	Hand = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Hand"));
	Hand->SetupAttachment(HandSocket);

	//DefaultSettings
	bUseControllerRotationYaw = false;
}

UBehaviorTree* const ABasicEnemyCharacter::GetBehaivorTree()
{
	return BHTree;
}

void ABasicEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	HandCondition = EHandState::Pistol;
}

void ABasicEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetAIDirection();
}

void ABasicEnemyCharacter::AIReload(float ReloadTime)
{
	SetActorTickEnabled(false);
	Hand->SetRelativeLocation(FVector(20, 0, 0));
	HandSocket->SetWorldRotation(FRotator(0, 90, 0));
	Hand->SetRelativeRotation(FRotator(0, 90, 90));
	Directionality = FVector2d(0, -1);
	HandCondition = EHandState::Reload;
	if(!ReloadTimer.IsValid())
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &ABasicEnemyCharacter::FinishReload, ReloadTime);
}

void ABasicEnemyCharacter::FinishReload()
{
	GetWorldTimerManager().ClearTimer(ReloadTimer);
	HandCondition = EHandState::Pistol;
	SetAIDirection();
	SetActorTickEnabled(true);
	Shoot();
}

void ABasicEnemyCharacter::Shoot()
{
	const FVector SpawnLocation = Hand->GetComponentLocation() + (HandSocket->GetForwardVector() * 20);
	const FVector SpawnLocation2 = FVector(SpawnLocation.X, SpawnLocation.Y, 40);
	const FTransform BulletTransform = FTransform(HandSocket->GetComponentRotation(), SpawnLocation2);
	FActorSpawnParameters SpawnParams;
	SpawnParams.Instigator = this;
	BulletRef = GetWorld()->SpawnActor<ABullet>(BulletType, BulletTransform, SpawnParams);
	OnShoot.Broadcast(BulletRef);
}

void ABasicEnemyCharacter::SetAIDirection()
{
	const AActor* PlayerCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	const FRotator Look = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), PlayerCharacter->GetActorLocation());
	HandSocket->SetWorldRotation(FRotator(0, Look.Yaw, 0), true);
	const int HandPos = UKismetMathLibrary::GridSnap_Float(HandSocket->GetComponentRotation().Yaw, 45);
		Hand->SetWorldRotation(FRotator(0, 180 - (HandPos - HandSocket->GetComponentRotation().Yaw), 90));
		switch(HandPos)
		{
		case 90:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(0, -1);
				break;
			}
		case 45:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(0.7, -0.7);
				break;
			}
		case 135:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(-0.7, -0.7);
				break;
			}
		case 180:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(-1, 0);
				break;
			}
		case -180:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(-20);
				Directionality = FVector2d(-1, 0);
				break;
			}
		case 0:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(1, 0);
				break;
			}
		case -90:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(0, 1);
				break;
			}
		case -45:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(0.7, 0.7);
				break;
			}
		case -135:
			{
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(-0.7, 0.7);
				break;
			}
		default:
			{
				break;
			}
		}
}

float ABasicEnemyCharacter::TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	Health -= 1;
	SpawnParticle(DamageCauser);
	if(Health <= 0)
	{
		Destroy();	
	}
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void ABasicEnemyCharacter::SpawnParticle_Implementation(AActor* Damager)
{}





