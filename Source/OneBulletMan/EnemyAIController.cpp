// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"

#include "BasicEnemyCharacter.h"
#include "BehaviorTree/BehaviorTree.h"

AEnemyAIController::AEnemyAIController()
{
	
}

void AEnemyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if(ABasicEnemyCharacter* const EnemyChar = Cast<ABasicEnemyCharacter>(InPawn))
	{
		if(UBehaviorTree* const BHTree = EnemyChar->GetBehaivorTree())
		{
			UBlackboardComponent* BBoard;
			UseBlackboard(BHTree->BlackboardAsset,BBoard);
			Blackboard = BBoard;
			RunBehaviorTree(BHTree);
		}
	}
}
