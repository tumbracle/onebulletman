// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "Jack.generated.h"

class USphereComponent;
class UPaperSpriteComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;
class ABullet;
class UPaperFlipbookComponent;

UENUM(BlueprintType)
enum class EDirection : uint8
{
	Down,
	DownRight,
	DownLeft,
	Up,
	UpRight,
	UpLeft,
	Right,
	Left
};

UENUM(BlueprintType)
enum class EHandState : uint8
{
	Pistol,
	Hand,
	Reload,
	PistolShoot
};
UCLASS()
class ONEBULLETMAN_API AJack : public APaperCharacter
{
	GENERATED_BODY()

	protected:

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputMappingContext* InputMapping;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* MouseAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* InteractAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* ShootAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* SprintAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* DashAction;

	UPROPERTY(EditAnywhere, Category = "EnhancedInput")
	UInputAction* GrabAction;

	int StopQueuers;

	public:
		// Sets default values for this character's properties
		AJack();

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly, Category = "Sprites")
	UPaperFlipbookComponent* Hand;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	USphereComponent* HandCollision;

	UPROPERTY(BlueprintReadOnly)
	FVector2D Directionality = FVector2D(0, -1);

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	USceneComponent* HandSocket;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	EDirection SpriteDir;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	EDirection MovementDir;

	UPROPERTY(BlueprintReadOnly, VisibleDefaultsOnly)
	EHandState HandCondition;

	UPROPERTY(BlueprintReadOnly)
	bool CanMove = true;

	UPROPERTY(BlueprintReadOnly)
	bool CanAim = false;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<ABullet> BulletType;

	UPROPERTY(BlueprintReadOnly)
	ABullet* BulletRef = nullptr;

	UPROPERTY(BlueprintReadOnly)
	bool InSprint;

	UPROPERTY(BlueprintReadOnly)
	bool InDash = false;

	UPROPERTY()
	float InitSpeed;

	UPROPERTY(EditDefaultsOnly)
	float RunAddSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float RunTire;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float DashTire;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float LooseSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float RestoreSpeed;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float RestoreValue;

	UPROPERTY(EditDefaultsOnly, Category = "Stamina")
	float RestoreDelay;

	FTimerHandle DashTimer;

	FTimerDelegate SprintDelegate;
		
	FTimerHandle SprintTimer;

	FTimerHandle RestoreStamina;

	FTimerDelegate RestoreDelegate;

	FTimerHandle ReloadTimer;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Dash")
	float DashDelay;

	UPROPERTY(BlueprintReadWrite, Category = "Stamina")
	float Stamina = 1000.f;

	bool CanShoot = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float Health = 3;

	float BulletsStore = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shooting")
	float ReloadDelay;
	
	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

		// Called to bind functionality to input
		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

		void Move(const FInputActionValue& Value);

		void SetHandPosition(const FInputActionValue& Value);

		void Interact(const FInputActionValue& Value);

		void Shoot(const FInputActionValue& Value);

public:

	//virtual float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator,
		//AActor* DamageCauser) override;
	
	// Called every frame
		virtual void Tick(float DeltaTime) override;

		UFUNCTION(BlueprintCallable)
		void PistolReady();

		UFUNCTION()
		void StopReload();

		void StartGrab();

		void EndGrab();

		UFUNCTION()    
		void OnHandOverlap(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

		void SetGlobalDirection();

		UFUNCTION(BlueprintNativeEvent)
		void SetPistolSprite();
		void SetPistolSprite_Implementation();

		void SetAimingDirection();

		UFUNCTION()
		void Dash(const FInputActionValue& Value);

		void EndDash();

		UFUNCTION(BlueprintCallable)
		void Aiming();

		void StartSprint(const FInputActionValue& Value);
		void EndSprint();

		UFUNCTION()
		void SetStaminaPercent(const float Value);

		UFUNCTION(BlueprintNativeEvent)
		void ShowStamina();
		void ShowStamina_Implementation();

		UFUNCTION(BlueprintNativeEvent)
		void HideStamina();
		void HideStamina_Implementation();

		void AddStopMoveQueuers();

		void RemoveStopMoveQueuers();

		UFUNCTION(BlueprintCallable)
		void SetReloadSpeed(float NewSpeed);

		UFUNCTION(BlueprintCallable)
		void AddAmmo(int Ammo);
};
