// Fill out your copyright notice in the Description page of Project Settings.


#include "Jack.h"
#include "Bullet.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
#include "PaperFlipbookComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AJack::AJack()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HandSocket = CreateDefaultSubobject<USceneComponent>(TEXT("Socket"));
	HandSocket->SetupAttachment(RootComponent);
	Hand = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("Hand0"));
	Hand->SetupAttachment(HandSocket);
	HandCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	HandCollision->SetupAttachment(Hand);
	HandCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void AJack::BeginPlay()
{
	Super::BeginPlay();
	if (GetCharacterMovement())
	InitSpeed = GetCharacterMovement()->MaxWalkSpeed;
	HandCollision->OnComponentBeginOverlap.AddDynamic(this, &AJack::OnHandOverlap);
	if (const APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(InputMapping, 0);
		}
	}
	
}

// Called every frame
void AJack::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AJack::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AJack::Move);
		EnhancedInputComponent->BindAction(MouseAction, ETriggerEvent::Triggered, this, &AJack::SetHandPosition);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AJack::Interact);
		EnhancedInputComponent->BindAction(ShootAction, ETriggerEvent::Started, this, &AJack::Shoot);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Triggered, this, &AJack::StartSprint);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Canceled, this, &AJack::EndSprint);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &AJack::EndSprint);
		EnhancedInputComponent->BindAction(DashAction, ETriggerEvent::Started, this, &AJack::Dash);
		EnhancedInputComponent->BindAction(GrabAction, ETriggerEvent::Started, this, &AJack::StartGrab);
		EnhancedInputComponent->BindAction(GrabAction, ETriggerEvent::Completed, this, &AJack::EndGrab);
		EnhancedInputComponent->BindAction(GrabAction, ETriggerEvent::Canceled, this, &AJack::EndGrab);
	}

}

void AJack::Move(const FInputActionValue& Value)
{
	const FVector2d InputVector = Value.Get<FVector2d>();
	if(IsValid(Controller) && CanMove)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation = FRotator(0, Rotation.Yaw, 0);

		const FVector ForwardVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector RightVector = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		AddMovementInput(ForwardVector, InputVector.X);
		AddMovementInput(RightVector, InputVector.Y);
		
		if(!CanAim)
		Directionality = FVector2d(InputVector.X, InputVector.Y * -1);
		else
		SetGlobalDirection();
	}
}

void AJack::SetHandPosition(const FInputActionValue& Value)
{
	if(CanAim)
	{
		SetGlobalDirection();
	}
}

void AJack::Interact(const FInputActionValue& Value)
{
	
}

void AJack::PistolReady()
{
	if(HandCondition == EHandState::Hand)
		EndGrab();
	Hand->SetRelativeLocation(FVector(20, 0, 0));
	HandSocket->SetWorldRotation(FRotator(0, 90, 0));
	Hand->SetRelativeRotation(FRotator(0, 90, 90));
	Hand->SetTranslucencySortDistanceOffset(-50);
	Directionality = FVector2d(0, -1);
	HandCondition = EHandState::Reload;
	AddStopMoveQueuers();
	Hand->SetHiddenInGame(false);
	if(!ReloadTimer.IsValid())
	GetWorldTimerManager().SetTimer(ReloadTimer, this, &AJack::StopReload, ReloadDelay, false);
}

void AJack::StopReload()
{
	GetWorldTimerManager().ClearTimer(ReloadTimer);
	SetGlobalDirection();
	Hand->SetHiddenInGame(false);
	HandCondition = EHandState::Pistol;
	if(BulletsStore == 0)
	{
		Hand->SetHiddenInGame(true);
		RemoveStopMoveQueuers();
	}
	else
	{
		CanShoot = true;
		CanAim = true;	
	}
}

void AJack::Shoot(const FInputActionValue& Value)
{
	if(CanShoot && BulletsStore != 0)
	{
		const FVector SpawnLocation = Hand->GetComponentLocation() + (HandSocket->GetForwardVector() * 20);
		const FVector SpawnLocation2 = FVector(SpawnLocation.X, SpawnLocation.Y, GetActorLocation().Z);
		const FTransform BulletTransform = FTransform(HandSocket->GetComponentRotation(), SpawnLocation2);
		FActorSpawnParameters SpawnParams;
		SpawnParams.Instigator = this;
		BulletRef = GetWorld()->SpawnActor<ABullet>(BulletType, BulletTransform, SpawnParams);
		Hand->SetHiddenInGame(true);
		BulletsStore -= 1;
		RemoveStopMoveQueuers();
		CanShoot = false;
		CanAim = false;
	}
}

void AJack::StartGrab()
{
	if(!CanShoot && HandCondition != EHandState::Reload)
	{
		HandCondition = EHandState::Hand;
		CanAim = true;
		SetGlobalDirection();
		Hand->SetHiddenInGame(false);
		HandCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);	
	}
}

void AJack::EndGrab()
{
	HandCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if(!CanShoot && HandCondition != EHandState::Reload)
	{
		CanAim = false;
		Hand->SetHiddenInGame(true);
	}
}

void AJack::OnHandOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(Cast<ABullet>(OtherActor))
	{
		BulletsStore += 1;
		PistolReady();
		OtherActor->Destroy();
	}
}

void AJack::SetGlobalDirection()
{
	FHitResult UnderMouseResult;
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, UnderMouseResult);
	const FRotator Look = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), UnderMouseResult.Location);
	HandSocket->SetWorldRotation(FRotator(0, Look.Yaw, 0), true);
	SetAimingDirection();
}

void AJack::SetPistolSprite_Implementation()
{
	
}

void AJack::SetAimingDirection()
{
		const int HandPos = UKismetMathLibrary::GridSnap_Float(HandSocket->GetComponentRotation().Yaw, 45);
		Hand->SetWorldRotation(FRotator(0, 180 - (HandPos - HandSocket->GetComponentRotation().Yaw), 90));
		switch(HandPos)
		{
		case 90:
			{
				SpriteDir = EDirection::Down;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(0, -1);
				break;
			}
		case 45:
			{
				SpriteDir = EDirection::DownRight;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(0.7, -0.7);
				break;
			}
		case 135:
			{
				SpriteDir = EDirection::DownLeft;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(-0.7, -0.7);
				break;
			}
		case 180:
			{
				SpriteDir = EDirection::Left;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(0);
				Directionality = FVector2d(-1, 0);
				break;
			}
		case -180:
			{
				SpriteDir = EDirection::Left;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(-20);
				Directionality = FVector2d(-1, 0);
				break;
			}
		case 0:
			{
				SpriteDir = EDirection::Right;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(1, 0);
				break;
			}
		case -90:
			{
				SpriteDir = EDirection::Up;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(0, 1);
				break;
			}
		case -45:
			{
				SpriteDir = EDirection::UpRight;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(0.7, 0.7);
				break;
			}
		case -135:
			{
				SpriteDir = EDirection::UpLeft;
				Hand->SetRelativeLocation(FVector(50, 0, 0));
				Hand->SetTranslucencySortDistanceOffset(20);
				Directionality = FVector2d(-0.7, 0.7);
				break;
			}
		default:
			{
				break;
			}
		}
}

void AJack::Aiming()
{
	if(!CanAim)
	{
		SetGlobalDirection();
		Hand->SetHiddenInGame(false);
	}
	else
	{
		Hand->SetHiddenInGame(true);
	}
	CanAim = !CanAim;
}

void AJack::Dash(const FInputActionValue& Value)
{
	if(!InDash && Stamina - DashTire >= 0 && CanMove)
	{
		GetWorldTimerManager().ClearTimer(RestoreStamina);
		AddStopMoveQueuers();
		InDash = true;
		GetWorldTimerManager().ClearTimer(SprintTimer);
		Stamina -= DashTire;
		ShowStamina();
		GetCharacterMovement()->MaxWalkSpeed = 0;
		const FVector DashVector = FVector(Directionality.X, Directionality.Y * -1, 0);
		GetCharacterMovement()->GroundFriction = 2;
		this->LaunchCharacter(DashVector * 1000, false, false);
		if (!DashTimer.IsValid())
			GetWorldTimerManager().SetTimer(DashTimer, this, &AJack::EndDash,DashDelay, false, -1);
	}
}

void AJack::EndDash()
{
	InDash = false;
	GetCharacterMovement()->GroundFriction = 100;
	GetCharacterMovement()->MaxWalkSpeed = InSprint ? InitSpeed + RunAddSpeed : InitSpeed;
	GetWorldTimerManager().ClearTimer(DashTimer);
	RemoveStopMoveQueuers();
	if(!RestoreStamina.IsValid())
	{
		RestoreDelegate.BindUFunction(this, FName("SetStaminaPercent"), RestoreValue);
		GetWorldTimerManager().SetTimer(RestoreStamina, RestoreDelegate, RestoreSpeed, true, RestoreDelay);
	}
}

void AJack::StartSprint(const FInputActionValue& Value)
{
	if (Stamina > 0 && !InDash && (GetVelocity().X != 0 || GetVelocity().Y != 0))
	{
		GetWorldTimerManager().ClearTimer(RestoreStamina);
		GetCharacterMovement()->MaxWalkSpeed = InitSpeed + RunAddSpeed;
		InSprint = true;
		if(!SprintTimer.IsValid())
		{
			SprintDelegate.BindUFunction(this, FName("SetStaminaPercent"), RunTire);
			GetWorldTimerManager().SetTimer(SprintTimer, SprintDelegate, LooseSpeed, true, 0);
		}
	}
	if(Stamina <= 0)
	{
		EndSprint();
	}
}

void AJack::EndSprint()
{
	GetWorldTimerManager().ClearTimer(SprintTimer);
	GetCharacterMovement()->MaxWalkSpeed = InitSpeed;
	InSprint = false;
	if(!RestoreStamina.IsValid())
	{
		RestoreDelegate.BindUFunction(this, FName("SetStaminaPercent"), RestoreValue);
		GetWorldTimerManager().SetTimer(RestoreStamina, RestoreDelegate, RestoreSpeed, true, RestoreDelay);
	}
}

void AJack::SetStaminaPercent(const float Value)
{
	//Stamina += Value;
	//if (Value < 0)
	//	Stamina = UKismetMathLibrary::Max(Stamina, 0);
	//else
	//	Stamina = UKismetMathLibrary::Min(Stamina, 10);
	Stamina = UKismetMathLibrary::Clamp(Stamina + Value, 0, 1000);
	ShowStamina();
	if (Stamina == 1000)
	{
		GetWorldTimerManager().ClearTimer(RestoreStamina);
		HideStamina();
	}
}

void AJack::ShowStamina_Implementation()
{
}

void AJack::HideStamina_Implementation()
{
}

// Disable movement system
void AJack::AddStopMoveQueuers()
{
	StopQueuers ++;
	CanMove = false;
}

void AJack::RemoveStopMoveQueuers()
{
	StopQueuers --;
	if(StopQueuers == 0)
		CanMove = true;
}

void AJack::SetReloadSpeed(float NewSpeed)
{
	ReloadDelay = NewSpeed;
}

void AJack::AddAmmo(int Ammo)
{
	BulletsStore += Ammo;
}
