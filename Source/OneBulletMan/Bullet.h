// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

class UPaperTileMap;
class UPaperFlipbook;
class UCapsuleComponent;
class UPaperSpriteComponent;
class USphereComponent;

UENUM(BlueprintType)
enum class EBulletType : uint8
{
	Basic,
	Fast,
	Small
};

UCLASS()
class ONEBULLETMAN_API ABullet : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UPaperSpriteComponent* Sprite;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USphereComponent* SphereCollision;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	USceneComponent* Scene;

	UPROPERTY(BlueprintReadOnly)
	EBulletType Type = EBulletType::Basic;

	UPROPERTY(BlueprintReadWrite)
	FVector MovementVector;

	UPROPERTY(BlueprintReadWrite)
	bool Enemy;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Health = 2;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int MaxHealth = 2;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int Damage = 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UPaperFlipbook* HitParticles;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<AActor> HitActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageType> TypeOfDamage;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
	void SpawnParticle();
	void SpawnParticle_Implementation();

	void Move();

	UFUNCTION(BlueprintCallable)
	void AddDamage();

	UFUNCTION(BlueprintCallable)
	void RestoreHealth();

	UFUNCTION(BlueprintNativeEvent)
	void DamageReaction(bool Restore);
	void DamageReaction_Implementation(bool Restore);

};
